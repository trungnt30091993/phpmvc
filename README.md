# Dự án Tìm hiểu mô hình PHP MVC .
 
## Cài đặt môi trường
Đầu tiên là cài Composer , Nodejs, Bower ...

Composer: truy cập link https://getcomposer.org/Composer-Setup.exe

NodeJS : https://nodejs.org/en/ chọn bản LTS ( bản ổn định)

Bower: dùng lệnh npm install -g bower (sau khi đã cài nodejs)


Sau đó thực hịện clone code và cài đặt gói thư viện sử dụng trong dự án

```
git clone https://gitlab.com/trungnt30091993/phpmvc.git
cd phpmvc // lệnh này là từ thư mục cha truy cập vào thư mục phpmvc 
composer install // cài toàn bộ các package có trong file composer.json
bower install 
```

## Thiết lập Cơ sở dữ liệu
tạo cơ sở dữ liệu vào cập nhật cơ sở  lưu trữ thông tin đăng nhập trong file `app/service.php`

## Tạo lược đồ
Thực hiện câu lệnh bên dưới ở trong thư mục dự án

```
php vendor/bin/doctrine orm:schema-tool:create
```


Quan trọng: 
    Đảm bảo Root gốc được đặt thành thư mục `public` trước khi bạn điều hướng đến máy chủ Mamp / Wamp của mình, 
    ví dụ: `http: //php-mvc.local: 8888`

Nếu bạn chưa cài đặt máy chủ Mamp / Wamp, bạn có thể muốn sử dụng máy chủ web tích hợp của PHP bằng lệnh sau:

```
php -S localhost:8888 -t public
```